using CharacteristicInvFourier
import Distributions
import Random: rand
using Test
import QuadGK: quadgk

const testperdist = 10


# L2 distance
function l2distance(f, g; interval::Tuple{T,T}=(-Inf, Inf)) where T<:Real
    integral, _ = quadgk(x -> (f(x) - g(x))^2, interval[1], interval[end])
    return integral
end


@testset "Normal distribution" begin
    for t=1:testperdist
        μ = 30 * 2 * (rand() - 0.5)
        σ = 30 * rand()
        normal_dist = Distributions.Normal(μ, σ)

        analytical_cdf = x -> Distributions.cdf(normal_dist, x)
        numerical_cdf = cdf_invfourier_fft(u -> Distributions.cf(normal_dist, u))
        @test l2distance(analytical_cdf, numerical_cdf) < 1e-14

        analytical_pdf = x -> Distributions.pdf(normal_dist, x)
        numerical_pdf = pdf_invfourier_fft(u -> Distributions.cf(normal_dist, u))
        @test l2distance(analytical_pdf, numerical_pdf) < 1e-14
    end
end


@testset "Exponential distribution" begin
    for t=1:testperdist
        θ = 20 * rand()
        exp_dist = Distributions.Exponential(θ)
        
        analytical_cdf = x -> Distributions.cdf(exp_dist, x)
        numerical_cdf = cdf_invfourier_fft(u -> Distributions.cf(exp_dist, u);
                                           npower=18, σrule=13
                                           )
        @test l2distance(analytical_cdf, numerical_cdf; interval=(0.0, Inf)) < 1e-11

        analytical_pdf = x -> Distributions.pdf(exp_dist, x)
        numerical_pdf = pdf_invfourier_fft(u -> Distributions.cf(exp_dist, u);
                                           npower=18, σrule=13
                                           )
        @test l2distance(analytical_pdf, numerical_pdf; interval=(0.0, Inf)) < 1e-5
    end
end


@testset "Gamma distribution" begin
    for t=1:testperdist
        α = 10 * rand()
        θ = 20 * rand()
        gamma_dist = Distributions.Gamma(α, θ)

        analytical_cdf = x -> Distributions.cdf(gamma_dist, x)
        numerical_cdf = cdf_invfourier_fft(u -> Distributions.cf(gamma_dist, u);
                                           npower=18, σrule=13
                                           )
        @test l2distance(analytical_cdf, numerical_cdf; interval=(0.0, Inf)) < 1e-9

        analytical_pdf = x -> Distributions.pdf(gamma_dist, x)
        numerical_pdf = pdf_invfourier_fft(u -> Distributions.cf(gamma_dist, u);
                                           npower=18, σrule=13
                                           )
        @test l2distance(analytical_pdf, numerical_pdf; interval=(0.0, Inf)) < 1e-3
    end
end


# EOF
