"""
For calculating the inverse Fourier transform of characteristic functions, to obtain
the corresponding probability density function and cumulative density function.

Based on:
Viktor Witkovský (2016) - Numerical inversion of a characteristic function: An alternative
tool to form the probability distribution of output quantity in linear measurement models
https://github.com/witkovsky/CharFunTool

written by:
Tomás Lobão de Almeida, 2022-2023
"""
module CharacteristicInvFourier


export pdf_invfourier_fft, cdf_invfourier_fft


import FFTW: fft
import Interpolations: Flat, linear_interpolation




"""
Returns a tuple (xmin, xmax) with the boundaries for the integration interval.
Approximated by using finite differences for calculating moments from the
characteristic function.
"""
function integration_interval(cf::F, σrule::Real, toldiff::Real) where {F<:Function}

    cf_derivs = cf.(toldiff * (1:4))

    xmean = real((-cf_derivs[2] + 8*cf_derivs[1] - 8*conj(cf_derivs[1])
                  + conj(cf_derivs[2])) / (12im*toldiff)
                 )

    xm2 = real(-(conj(cf_derivs[4]) - 16*conj(cf_derivs[3])
                 + 64*conj(cf_derivs[2]) + 16*conj(cf_derivs[1])
                 - 130 + 16*cf_derivs[1] + 64*cf_derivs[2]
                 - 16*cf_derivs[3] + cf_derivs[4])
               / (144*(toldiff^2))
               )
    
    xstd = sqrt(xm2 - (xmean^2))
        
    xmin = xmean - σrule * xstd
    xmax = xmean + σrule * xstd

    return (xmin, xmax)
end




"""
Returns integration objects of the provided characteristic function Φ as the result of
applying the fft algorithm to a discretized grid. These are used to obtain either the
pdf or cdf by performing some transformations.
"""
function integration_invfourier_fft(Φ::F; npower::Int=15,
                                    σrule::Real=10, toldiff::Real=1e-4,
                                    xbounds::Union{Nothing, Tuple{T,T}, Vector{T}}=nothing,
                                    inf_atzero::Bool=false
                                    ) where {F<:Function, T<:Real
                                             }

    # rescale Φ for calculations if there's infinite density at zero
    if inf_atzero
        cfcalc::F = (u::Real) -> (Φ(u) - real(Φ(1e30))) / (1 - real(Φ(1e30)))
    else
        cfcalc = Φ
    end

    N = 2 ^ npower  # number of grid points

    if isnothing(xbounds)
        xinterval = integration_interval(cfcalc, σrule, toldiff)
    else
        xinterval = xbounds
    end

    xmin = xinterval[1]
    xrange = xinterval[end] - xinterval[begin]
    dx = xrange / N
    
    # create grid in fourier space u
    # k = 0:1:(N-1)
    k = LinRange(0, N-1, N)
    du = 2π / xrange
    u = ((0.5 - (N/2)) .+ k) * du

    # create grid in probability space x
    x = xmin .+ k * dx

    # calculate c.f. at grid points and mirror for negative imaginary axis
    cfu = cfcalc.(u[Int((N/2)+1):end])
    cfu = vcat(conj.(cfu[end:-1:begin]), cfu)
    
    # integration with fft
    C = @. ((-1+0im) ^ ( (1 - (1/N)) * ((xmin/dx) + k))) / xrange
    D = (-1+0im) .^ (-2 * (xmin / xrange) * k)

    return u, x, cfu, C, D
end




"""
Returns the pdf of the provided characteristic function Φ as the result of performing an
inverse Fourier transform using fft and a discretized grid. The function returned is a
linear interpolation of the resulting grid.
"""
function pdf_invfourier_fft(Φ::F; npower::Int=15, σrule::Real=10, toldiff::Real=1e-4,
                            xbounds::Union{Nothing, Tuple{T,T}, Vector{T}}=nothing,
                            inf_atzero::Bool=false
                            ) where {F<:Function, T<:Real
                                     }

    _, x, cfu, C, D = integration_invfourier_fft(Φ; npower=npower, σrule=σrule,
                                                 toldiff=toldiff,
                                                 xbounds=xbounds
                                                 )
    
    pdf_grid = map(x -> max(0.0, x), real(C .* fft(D .* cfu)))
    
    if real(Φ(Inf)) > 0
        pdf_grid = pdf_grid * (1 - real(Φ(1e30)))
        pdf_grid[findfirst(x->x==0)] = Inf
    end

    pdf = linear_interpolation(x, pdf_grid, extrapolation_bc=zero(eltype(pdf_grid)))

    return pdf
end




"""
Returns the pdf of the provided characteristic function Φ as the result of performing an
inverse Fourier transform using fft and a discretized grid. The function returned is a
linear interpolation of the resulting grid.
"""
function cdf_invfourier_fft(Φ::F; npower::Int=15, σrule::Real=10, toldiff::Real=1e-4,
                            xbounds::Union{Nothing, Tuple{T,T}, Vector{T}}=nothing,
                            inf_atzero::Bool=false
                            ) where {F<:Function, T<:Real
                                     }

    u, x, cfu, C, D = integration_invfourier_fft(Φ; npower=npower, σrule=σrule,
                                                 toldiff=toldiff,
                                                 xbounds=xbounds
                                                 )

    cdf_grid = map(x -> min(1.0, x), 0.5 .+ real(1im * C .* fft(@. D * cfu / u)))
    
    if real(Φ(Inf)) > 0
        cdf_grid = real(Φ(1e30)) .+  cdf_grid * (1 - real(Φ(1e30)))
    end

    N = length(x)

    # have overlap between [N/2 - 1, N/2 + 1] for safety
    
    cdf_left = linear_interpolation(x[begin:Int(1 + N/2)],
                                    cdf_grid[begin:Int(1 + N/2)],
                                    extrapolation_bc=zero(eltype(cdf_grid))
                                    )
    
    cdf_right = linear_interpolation(x[Int(N/2 - 1):end],
                                    cdf_grid[Int(N/2 - 1):end],
                                    extrapolation_bc=one(eltype(cdf_grid))
                                    )

    function cdf_bybranches(uval::U) where {U<:Real}
        value = zero(U)
        if uval < x[Int(N/2)]
            value = cdf_left(uval)
        elseif uval >= x[Int(N/2)]
            value = cdf_right(uval)
        end
        return value
    end
    
    return cdf_bybranches
end




end # module
